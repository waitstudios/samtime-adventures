extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Button_pressed():
	get_tree().change_scene("scenes/Run.tscn")


func _on_QuitButton_pressed():
	get_tree().quit()


func _on_AcceptDialog_confirmed():
	get_node("AcceptDialog").popup()
