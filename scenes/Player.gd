extends KinematicBody2D

const GRAVITY_VEC = Vector2(0, 900)
const FLOOR_NORMAL = Vector2(0, -1)
const SLOPE_SLIDE_STOP = 25.0
const WALK_SPEED = 250
const JUMP_SPEED = 480

var motion = Vector2()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	motion += delta * GRAVITY_VEC
	motion = move_and_slide(motion, FLOOR_NORMAL, SLOPE_SLIDE_STOP)
	
	if is_on_floor() and Input.is_action_just_pressed("ui_up"):
		motion.y = -JUMP_SPEED
	
	var target_speed = 0
	if Input.is_action_pressed("ui_right"):
		target_speed += WALK_SPEED
		$AnimatedSprite.set_flip_h(false)
	if Input.is_action_pressed("ui_left"):
		target_speed -= WALK_SPEED
		$AnimatedSprite.set_flip_h(true)
	motion.x = lerp(motion.x, target_speed, 0.1)
	if motion.y > 1000:
		get_tree().change_scene("scenes/GameOver.tscn")

func _on_Button_pressed():
	get_node("Popup")._ready()
