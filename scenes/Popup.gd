extends Popup
export var text = ""
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("RichTextLabel").text = text
	get_tree().paused = true
	popup()
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_TextureButton_pressed():
	get_tree().paused = false
	self.hide()

func _on_Popup_popup_hide():
	get_tree().paused = false # Replace with function body.
